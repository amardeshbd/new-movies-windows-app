﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/** 
 * JSON Mapping Object generated from: http://json2csharp.com/ 
 */
namespace NewMovieReviews.Models
{

    public class Review
    {
        public string critic { get; set; }
        public string date { get; set; }
        public string freshness { get; set; }
        public string publication { get; set; }
        public string quote { get; set; }
        public Links links { get; set; }
        public string original_score { get; set; }
    }

    public class MovieReviewsJsonWrapper
    {
        public int total { get; set; }
        public List<Review> reviews { get; set; }
        public Links2 links { get; set; }
        public string link_template { get; set; }
    }

    public class AbridgedDirector
    {
        public string name { get; set; }
    }

    public class MovieDetailsJsonWrapper
    {
        public int id { get; set; }
        public string title { get; set; }
        public int year { get; set; }
        public List<string> genres { get; set; }
        public string mpaa_rating { get; set; }
        public int runtime { get; set; }
        public string critics_consensus { get; set; }
        public ReleaseDates release_dates { get; set; }
        public Ratings ratings { get; set; }
        public string synopsis { get; set; }
        public Posters posters { get; set; }
        public List<AbridgedCast> abridged_cast { get; set; }
        public List<AbridgedDirector> abridged_directors { get; set; }
        public string studio { get; set; }
        public AlternateIds alternate_ids { get; set; }
        public Links links { get; set; }
    }
}
