﻿using System;
using System.Net;
using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text;
using NewMovieReviews.ViewModels;


namespace NewMovieReviews.Models
{
    public class Movies
    {
        public enum ListType { Opening, InTheaters, Upcoming };
        public static readonly String RatingsHigh = "/Assets/Fresh.png";
        public static readonly String RatingsLow = "/Assets/Rotten.png";
        
        public static readonly String ApiKey = "j2qjvxu5gwd9s5zhyy24jnx5";
        private static readonly short MoviesLimit = 20;
        private static readonly String TheatersUrl =
            "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?"
            + "limit=" + MoviesLimit + "&apikey=" + ApiKey;

        private static readonly String UpcomingUrl =
                  "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/upcoming.json?"
                  + "limit=" + MoviesLimit + "&apikey=" + ApiKey;

        private static readonly String OpeningUrl =
                  "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/opening.json?"
                  + "limit=" + MoviesLimit + "&apikey=" + ApiKey;



        private static Boolean inTheatersLoaded = false;
        private static Boolean openingLoaded = false;
        private static Boolean upcomingLoaded = false;

        private static MovieJsonWrapper inTheatersMovies;
        private static MovieJsonWrapper openingMovies;
        private static MovieJsonWrapper upcomingMovies;

        public MovieJsonWrapper InTheaters
        {
            get
            {
                if (inTheatersLoaded)
                {
                    return inTheatersMovies;
                }
                return null;
            }
            
        }

        public MovieJsonWrapper Upcoming
        {
            get
            {
                if (upcomingLoaded)
                {
                    return upcomingMovies;
                }
                return null;
            }

        }

        public MovieJsonWrapper Opening
        {
            get
            {
                if (openingLoaded)
                {
                    return openingMovies;
                }
                return null;
            }

        }


        /**
         * Checks if all the Movie Items are loaded or not 
         */
        public Boolean IsLoaded()
        {
            return inTheatersLoaded && openingLoaded && upcomingLoaded;
        }


        /**
         * Load all the Movie Data Asynchronously from the Rotten Tomatoes Server :)
         * @param LoadItemsCallBack a call back method.
         * 
         */
        public Movies(MainViewModel.LoadItemsCallBack load)
        {
            /* load inTheaters async */
            if (!inTheatersLoaded)
            {
                // load the Movies Async and then set inTheaters Loaded to true
                WebClient client = new WebClient();
                client.DownloadStringCompleted += 
                       new DownloadStringCompletedEventHandler (
                            delegate(object sender, DownloadStringCompletedEventArgs e)
                            {
                                try
                                {
                                    inTheatersMovies = JsonConvert.DeserializeObject<MovieJsonWrapper>(e.Result);
                                    inTheatersLoaded = true;
                                    load(inTheatersMovies, ListType.InTheaters);
                                }
                                catch (Exception exp)
                                {
                                    System.Diagnostics.Debug.WriteLine("Error >> " + exp.Message);
                                    load(null, ListType.InTheaters);    
                                }

                            }
                        );
                client.DownloadStringAsync(new Uri(Movies.TheatersUrl));
            }

            /* load openingMovies async */
            if (!openingLoaded)
            {
                // load the Movies Async and then set inTheaters Loaded to true
                WebClient client = new WebClient();
                client.DownloadStringCompleted +=
                        new DownloadStringCompletedEventHandler(
                            delegate(object sender, DownloadStringCompletedEventArgs e)
                            {
                                try
                                {
                                    openingMovies = JsonConvert.DeserializeObject<MovieJsonWrapper>(e.Result);
                                    openingLoaded = true;
                                    load(openingMovies, ListType.Opening);
                                }
                                catch (Exception exp)
                                {
                                    System.Diagnostics.Debug.WriteLine("Error >> " + exp.Message);
                                    load(null, ListType.Opening);
                                }
                            }
                        );
                client.DownloadStringAsync(new Uri(Movies.OpeningUrl));
            }

            /* load upcomingMovies async */
            if (!upcomingLoaded)
            {
                // load the Movies Async and then set inTheaters Loaded to true
                WebClient client = new WebClient();
                client.DownloadStringCompleted +=
                        new DownloadStringCompletedEventHandler(
                            delegate(object sender, DownloadStringCompletedEventArgs e)
                            {
                                try
                                {
                                    upcomingMovies = JsonConvert.DeserializeObject<MovieJsonWrapper>(e.Result);
                                    upcomingLoaded = true;
                                    load(upcomingMovies, ListType.Upcoming);
                                }
                                catch (Exception exp)
                                {
                                    System.Diagnostics.Debug.WriteLine("Error >> " + exp.Message);
                                    load(null, ListType.Upcoming);
                                }
                            }
                        );
                client.DownloadStringAsync(new Uri(Movies.UpcomingUrl));
            }

        }

        public static DateTime ReleaseDate(String date)
        {
            DateTime rDate = DateTime.ParseExact(date, "yyyy-MM-dd", null);

            return rDate;           
        }

        public static TimeSpan TimeSpanDifference(DateTime releaseDate)
        {
            DateTime today = DateTime.Now;
            return releaseDate - today;
        }

        public static String FormatReleaseDate(DateTime dt)
        {
            return String.Format("{0:MMMM d}", dt);
        }
    }
}
