﻿using NewMovieReviews.Models;
using NewMovieReviews.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NewMovieReviews.Models
{
    class MovieDetails
    {
        private static Dictionary<String, MovieDetailsJsonWrapper> movieDict;
        private static Dictionary<String, MovieReviewsJsonWrapper> reviewsDict;

        /* Creates an new MovieDetails with empty Dictonary */
        public MovieDetails()
        {
            if (movieDict == null)
            {
                movieDict = new Dictionary<String, MovieDetailsJsonWrapper>();
            }
            if (reviewsDict == null)
            {
                reviewsDict = new Dictionary<String, MovieReviewsJsonWrapper>();
            }
        }

        /** 
         * <summary>Makes an async request with the movie id to get the movie details from
         *   rotten tomatoes Server, and then invokes the call back method provided on the 
         *   parameter with the returned data from server as an Object (MovieDetailsWrapper)
         *  <param>id - The id of the movie </param>
         *  <param>callBack - The callback function </param>
         * </summary>
         */
        public void getMovieDetailsAsync(DetailsViewModel.DetailsCallBack callback, String id)
        {
            if (movieDict == null)
            {
                movieDict = new Dictionary<String, MovieDetailsJsonWrapper>();    
            }

            if (movieDict.ContainsKey(id))
            {
                callback(movieDict[id]);
            }
            else
            {
                // make an async request to get the key and then invoke the callback method
                WebClient client = new WebClient();
                client.DownloadStringCompleted +=
                       new DownloadStringCompletedEventHandler(
                            delegate(object sender, DownloadStringCompletedEventArgs e)
                            {
                                if (e.Result != null)
                                {
                                    MovieDetailsJsonWrapper detailsWrapper = JsonConvert.DeserializeObject<MovieDetailsJsonWrapper>(e.Result);
                                    movieDict.Add(id, detailsWrapper);
                                    callback(movieDict[id]);
                                }
                            }
                        );
                client.DownloadStringAsync(new Uri(this.buildDetailsUrl(id), UriKind.Absolute));
            }

        }


        /**
         * <summary>Builds the URL of the query to be fetched</summary>
         * 
         */
        private String buildDetailsUrl(String id)
        {
            String detailsUrl = "http://api.rottentomatoes.com/api/public/v1.0/movies/" + id + ".json?apikey=" + Movies.ApiKey;

            return detailsUrl;
        }


        /** 
         * <summary>Makes an async request with the movie id to get the movie Reviews from
         *   rotten tomatoes Server, and then invokes the call back method provided on the 
         *   parameter with the returned data from server as an Object (MovieDetailsWrapper)
         * </summary>  
         *  <param>id - The id of the movie </param>
         *  <param>callBack - The callback function </param>
         *
         */
        public void getMovieReviewsAsync(DetailsViewModel.ReviewsCallBack callback, String id)
        {
            if (reviewsDict == null)
            {
                reviewsDict = new Dictionary<String, MovieReviewsJsonWrapper>();
            }

            if (reviewsDict.ContainsKey(id))
            {
                callback(reviewsDict[id]);
            }
            else
            {
                // make an async request to get the key and then invoke the callback method
                WebClient client = new WebClient();
                client.DownloadStringCompleted +=
                       new DownloadStringCompletedEventHandler(
                            delegate(object sender, DownloadStringCompletedEventArgs e)
                            {
                                MovieReviewsJsonWrapper reviewsWrapper = JsonConvert.DeserializeObject<MovieReviewsJsonWrapper>(e.Result);
                                reviewsDict.Add(id, reviewsWrapper);
                                callback(reviewsDict[id]);
                            }
                        );
                client.DownloadStringAsync(new Uri(this.buildReviewsUrl(id), UriKind.Absolute));
            }

        }


        /**
         * <summary>Builds the URL of the query to be fetched</summary>
         * 
         */
        private String buildReviewsUrl(String id)
        {
            String reviewsUrl = "http://api.rottentomatoes.com/api/public/v1.0/movies/" 
                                 + id + "/reviews.json?apikey=" + Movies.ApiKey;

            return reviewsUrl;
        }

        /**
         * <summary>
         * Gets the Movie Info of a Movie (based on its Id)
         * </summary>
         * <param name="id">The Id of the Movie</param>
         */
        public static MovieDetailsJsonWrapper GetMovie(String id)
        {
            if (movieDict == null)
            {
                return null;
            }

            // guaranteed to have the id in the movieDict
            return movieDict[id];
        }

    }
}
