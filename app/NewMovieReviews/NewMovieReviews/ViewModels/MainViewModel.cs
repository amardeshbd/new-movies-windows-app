﻿#define Debug

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using NewMovieReviews.Resources;
using System.Collections.Generic;
using System.Text;
using NewMovieReviews.Models;
using System.Globalization;
using NewMovieReviews.Model;
using System.Linq;



namespace NewMovieReviews.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private Movies movies;


        /**
         * maintain a collection of various Movie Items (InTheaters, Opening and Upcoming)
         * as well as Watchlist Items
         */
        public ObservableCollection<ItemViewModel> InTheatersItems { get; private set; }
        public ObservableCollection<ItemViewModel> OpeningItems { get; private set; }
        public ObservableCollection<ItemViewModel> UpcomingItems { get; private set; }
        public ObservableCollection<ItemViewModel> WatchlistItems { get; set; }

        /* A callback which is invoked after loading is done asynchronously (for Opening, Upcoming, and InTheaters) 
         * from Rotten Tomatoes Server */
        public delegate void LoadItemsCallBack(MovieJsonWrapper movieWrapper, Movies.ListType type);
        private static MainPage.LoadErrorCallback errorCallBack;

        // LINQ to SQL data context for the local database.
        private static WatchlistDataContext watchlistDB;

        private static MainViewModel instance;

        public static MainViewModel getInstance()
        {
            if (instance == null)
            {
                instance = new MainViewModel();
            }
            return instance;
        }

        private MainViewModel()
        {           
            InTheatersItems = new ObservableCollection<ItemViewModel>();
            OpeningItems = new ObservableCollection<ItemViewModel>();
            UpcomingItems = new ObservableCollection<ItemViewModel>();
            WatchlistItems = new ObservableCollection<ItemViewModel>();

            watchlistDB = new WatchlistDataContext(App.DBConnection);

           // System.Diagnostics.Debug.WriteLine("Movies -- Begin Development");
        }

        /**
         * Reloads the content/InTheaters, Upcoming, Opening Items 
         * from the Server */
        public void Reload()
        {
            this.LoadData(errorCallBack);
        }

        public static String GetCasts(List<AbridgedCast> castList)
        {
            const int MAX_CAST = 2;  
            StringBuilder casts = new StringBuilder();
            int i = 0;
            foreach (AbridgedCast cast in castList)
            {
                if (i < MAX_CAST)
                {
                    casts.Append(cast.name).Append(", ");
                }
                i++;
            }
            // only trim if there are more than 1 casts items
            if (i > 1) {
                casts.Length = casts.Length - 2;
            }

            return casts.ToString();
        }

        public void loadItems(MovieJsonWrapper movieWrapper, Movies.ListType type) 
        {
            if (movieWrapper == null)
            {
                errorCallBack("Error Loading Movies. Please Refresh to Try Again");
                return;
            }

            foreach (Movie movie in movieWrapper.movies)
            {
                String casts = GetCasts(movie.abridged_cast);

                // convert release date to a format without year
                String releaseDate = movie.release_dates.theater;
                System.Diagnostics.Debug.WriteLine("Raw Date >> " + movie.release_dates.theater);
                try {
                    DateTime dt = Movies.ReleaseDate(movie.release_dates.theater);
                    releaseDate = Movies.FormatReleaseDate(dt);
                    TimeSpan diff = Movies.TimeSpanDifference(dt);

                }
                catch (FormatException fe)
                {
                    System.Diagnostics.Debug.WriteLine("Error Message: " + fe.Message + "Date >>" + movie.release_dates.theater);
                    
                }

                System.Diagnostics.Debug.WriteLine("Release Date >> " + releaseDate);

                String ratingImage = movie.ratings.audience_score >= 50 ? Movies.RatingsHigh : Movies.RatingsLow;


                if (type == Movies.ListType.Opening)
                {
                    OpeningItems.Add(new ItemViewModel() {
                        LineId = movie.id,
                        LineOpeningImage = movie.posters.profile,
                        LineOpeningTitle = movie.title,
                        LineOpeningCast = casts,
                        LineOpeningGenre = releaseDate,
                        LineOpeningRating = movie.ratings.audience_score + "%",
                        LineRatingImage = ratingImage
                    });
                }
                else if (type == Movies.ListType.InTheaters)
                {
                    InTheatersItems.Add(new ItemViewModel()
                    {
                        LineId = movie.id,
                        LineTheatersImage = movie.posters.profile,
                        LineTheatersTitle = movie.title,
                        LineTheatersCast = casts,
                        LineTheatersGenre = releaseDate,
                        LineTheatersRating = movie.ratings.audience_score + "%",
                        LineRatingImage = ratingImage
                    });

                }
                else
                {
                    UpcomingItems.Add(new ItemViewModel()
                    {
                        LineId = movie.id,
                        LineUpcomingImage = movie.posters.profile,
                        LineUpcomingTitle = movie.title,
                        LineUpcomingCast = casts,
                        //  LineOpeningCast = String.Join(",", movie.abridged_cast.ToArray()), //."Maecenas praesent ", 
                        LineUpcomingGenre = releaseDate, // + " (in " + diff.Days + " days)",
                        LineUpcomingRating = movie.ratings.audience_score + "%",
                        LineRatingImage = ratingImage
                    });


                }
            }
            NotifyPropertyChanged("WatchlistItems");
            //LoadWatchListItems();
        }


        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>

        private string _sampleProperty = "Sample Runtime Property Value";
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>
        public string SampleProperty
        {
            get
            {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        /// <summary>
        /// Sample property that returns a localized string
        /// </summary>
        public string LocalizedSampleProperty
        {
            get
            {
                return AppResources.SampleProperty;
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }


        public void AddWatchlistItem(String id)        
        {
            MovieDetailsJsonWrapper movieDetails = MovieDetails.GetMovie(id);
            String casts = GetCasts(movieDetails.abridged_cast);

            // check if item already exists in database, then simply ignore the addition

            String ratingImage = movieDetails.ratings.audience_score >= 50 ? Movies.RatingsHigh : Movies.RatingsLow;
            ItemViewModel newModel = new ItemViewModel()
            {
                LineId = Convert.ToInt32(id) + "",
                LineWatchlistTitle = movieDetails.title,
                LineWatchlistCast = casts,
                LineWatchlistRating = movieDetails.ratings.audience_score + "%",
                LineWatchlistGenre = Movies.FormatReleaseDate(Movies.ReleaseDate(movieDetails.release_dates.theater)),
                LineWatchlistImage = movieDetails.posters.profile,
                LineRatingImage = ratingImage

            };

            // check if the item already exists, then simply ignore the request!
            if (WatchlistItems.Contains(newModel))
            {
                System.Diagnostics.Debug.WriteLine("Found the item -- DO NOT ADD");
                return;
            }


            MovieItem newItem = new MovieItem()
            {
                MovieItemId = Convert.ToInt32(id),
                ItemTitle = movieDetails.title,
                ItemCasts = casts,
                ItemImage = movieDetails.posters.profile,
                ItemRating = movieDetails.ratings.audience_score + "",
                ItemReleaseDate = movieDetails.release_dates.theater //Movies.FormatReleaseDate(Movies.ReleaseDate(movieDetails.release_dates.theater))
            };
            /* insert to DB and update changes */
            if (watchlistDB == null)
            {
                watchlistDB = new WatchlistDataContext(App.DBConnection);                
            }
            watchlistDB.Items.InsertOnSubmit(newItem);
            watchlistDB.SubmitChanges();
//            watchlistDB.Refresh(;

            /* Also update the existing WatchlistItems */
            WatchlistItems.Add(newModel);

            if (WatchlistItems.Contains(newModel))
            {
                System.Diagnostics.Debug.WriteLine("Found the item -- added to Watchlist");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error: Item was not added to Watchlist");
            }

            //LoadWatchListItems();
            watchlistDB = null;
        }


        public void LoadWatchListItems()
        {
            // now load the Watchlist from local database/storage!
            // Specify the query for all to-do items in the database.
            var WatchlistInDB = from MovieItem movieItem in watchlistDB.Items
                                select movieItem;

            // Query the database and load all items in watchlist.
            WatchlistItems.Clear();
            foreach (MovieItem item in WatchlistInDB)
            {
                String ratingImage = Convert.ToInt32(item.ItemRating) >= 50 ? Movies.RatingsHigh : Movies.RatingsLow;
                WatchlistItems.Add(new ItemViewModel()
                {
                    LineId = item.MovieItemId + "",
                    LineWatchlistTitle = item.ItemTitle,
                    LineWatchlistCast = item.ItemCasts,
                    LineWatchlistRating = item.ItemRating + "%",
                    LineWatchlistGenre = Movies.FormatReleaseDate(Movies.ReleaseDate(item.ItemReleaseDate)),
                    LineWatchlistImage = item.ItemImage,
                    LineRatingImage = ratingImage
                });
            }

        }


        /**
         * <summary>Checks if an an Movie with id is contained in the watch list
         * </summary>
         * <param name="id">The id of the Movie</param>
         */
        public Boolean WatchlistContains(String id)
        {
            MovieDetailsJsonWrapper movieDetails = MovieDetails.GetMovie(id);
            String ratingImage = movieDetails.ratings.audience_score >= 50 ? Movies.RatingsHigh : Movies.RatingsLow;

            String casts = GetCasts(movieDetails.abridged_cast);
            ItemViewModel newModel = new ItemViewModel()
            {
                LineId = Convert.ToInt32(id) + "",
                LineWatchlistTitle = movieDetails.title,
                LineWatchlistCast = casts,
                LineWatchlistRating = movieDetails.ratings.audience_score + "%",
                LineWatchlistGenre = Movies.FormatReleaseDate(Movies.ReleaseDate(movieDetails.release_dates.theater)),
                LineWatchlistImage = movieDetails.posters.profile,
                LineRatingImage = ratingImage
            };

            return WatchlistItems.Contains(newModel);
        }



        public void DeleteWatchlistItem(String id)
        {
            MovieDetailsJsonWrapper movieDetails = MovieDetails.GetMovie(id);
            String casts = GetCasts(movieDetails.abridged_cast);

            /*
            MovieItem deleteItem = new MovieItem()
            {
                MovieItemId = Convert.ToInt32(id),
                ItemTitle = movieDetails.title,
                ItemCasts = casts,
                ItemImage = movieDetails.posters.profile,
                ItemRating = movieDetails.ratings.audience_score + "",
                ItemReleaseDate = movieDetails.release_dates.theater //Movies.FormatReleaseDate(Movies.ReleaseDate(movieDetails.release_dates.theater))
            };
            */
            System.Diagnostics.Debug.WriteLine("Item to be deleted: >> " + id);

            if (watchlistDB == null)
            {
                watchlistDB = new WatchlistDataContext(App.DBConnection);
            }

            //var deleteItem = watchlistDB.Items.Single(
            MovieItem deleteItem = watchlistDB.Items.Single(p => p.MovieItemId.Equals(Convert.ToInt32(id)));
            watchlistDB.Items.DeleteOnSubmit(deleteItem);
            watchlistDB.SubmitChanges();
            watchlistDB = null;

            String ratingImage = movieDetails.ratings.audience_score >= 50 ? Movies.RatingsHigh : Movies.RatingsLow;
            ItemViewModel deleteModel = new ItemViewModel()
            {
                LineId = Convert.ToInt32(id) + "",
                LineWatchlistTitle = movieDetails.title,
                LineWatchlistCast = casts,
                LineWatchlistRating = movieDetails.ratings.audience_score + "%",
                LineWatchlistGenre = Movies.FormatReleaseDate(Movies.ReleaseDate(movieDetails.release_dates.theater)),
                LineWatchlistImage = movieDetails.posters.profile,
                LineRatingImage = ratingImage
            };

            WatchlistItems.Remove(deleteModel);
        }


        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData(MainPage.LoadErrorCallback errorMessageCallback)
        {
            LoadItemsCallBack callBack = new LoadItemsCallBack(this.loadItems);
            this.movies = new Movies(callBack);          
            this.IsDataLoaded = true;
            
            if (errorCallBack == null)
            {
                errorCallBack = errorMessageCallback;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        
    }
}