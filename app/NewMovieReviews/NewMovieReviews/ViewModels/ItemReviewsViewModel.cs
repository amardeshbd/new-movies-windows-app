﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMovieReviews.ViewModels
{
    public class ItemReviewsViewModel : INotifyPropertyChanged
    {

        private string _lineName;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineName
        {
            get
            {
                return _lineName;
            }
            set
            {
                if (value != _lineName)
                {
                    _lineName = value;
                    NotifyPropertyChanged("LineName");
                }
            }
        }


        private string _lineComment;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineComment
        {
            get
            {
                return _lineComment;
            }
            set
            {
                if (value != _lineComment)
                {
                    _lineComment = value;
                    NotifyPropertyChanged("LineComment");
                }
            }
        }

        private string _lineReviewDate;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineReviewDate
        {
            get
            {
                return _lineReviewDate;
            }
            set
            {
                if (value != _lineReviewDate)
                {
                    _lineReviewDate = value;
                    NotifyPropertyChanged("LineReviewDate");
                }
            }
        }


        private string _lineReviewRating;

        public string LineReviewRating
        {
            get
            {
                return _lineReviewRating;
            }
            set
            {
                if (value != _lineReviewRating)
                {
                    _lineReviewRating = value;
                    NotifyPropertyChanged("LineReviewRating");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public ItemReviewsViewModel()
        {

        }

    }
}
