﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace NewMovieReviews.ViewModels
{
    public class ItemViewModel : INotifyPropertyChanged, IEquatable<ItemViewModel>
    {

        private string _lineId;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineId
        {
            get
            {
                return _lineId;
            }
            set
            {
                if (value != _lineId)
                {
                    _lineId = value;
                    NotifyPropertyChanged("LineId");
                }
            }
        }


        private string _lineRatingImage;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineRatingImage
        {
            get
            {
                return _lineRatingImage;
            }
            set
            {
                if (value != _lineRatingImage)
                {
                    _lineRatingImage = value;
                    NotifyPropertyChanged("LineRatingImage");
                }
            }
        }


        private string _lineOpeningTitle;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineOpeningTitle
        {
            get
            {
                return _lineOpeningTitle;
            }
            set
            {
                if (value != _lineOpeningTitle)
                {
                    _lineOpeningTitle = value;
                    NotifyPropertyChanged("LineOpeningTitle");
                }
            }
        }

        private string _lineOpeningImage;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineOpeningImage
        {
            get
            {
                return _lineOpeningImage;
            }
            set
            {
                if (value != _lineOpeningImage)
                {
                    _lineOpeningImage = value;
                    NotifyPropertyChanged("LineOpeningImage");
                }
            }
        }

        private string _lineOpeningCast;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineOpeningCast
        {
            get
            {
                return _lineOpeningCast;
            }
            set
            {
                if (value != _lineOpeningCast)
                {
                    _lineOpeningCast = value;
                    NotifyPropertyChanged("LineOpeningCast");
                }
            }
        }

        private string _lineOpeningGenre;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineOpeningGenre
        {
            get
            {
                return _lineOpeningGenre;
            }
            set
            {
                if (value != _lineOpeningGenre)
                {
                    _lineOpeningGenre = value;
                    NotifyPropertyChanged("LineOpeningGenre");
                }
            }
        }

        private string _lineOpeningRating;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineOpeningRating
        {
            get
            {
                return _lineOpeningRating;
            }
            set
            {
                if (value != _lineOpeningRating)
                {
                    _lineOpeningRating = value;
                    NotifyPropertyChanged("LineOpeningRating");
                }
            }
        }


        private string _lineTheatersTitle;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineTheatersTitle
        {
            get
            {
                return _lineTheatersTitle;
            }
            set
            {
                if (value != _lineTheatersTitle)
                {
                    _lineTheatersTitle = value;
                    NotifyPropertyChanged("LineTheatersTitle");
                }
            }
        }

        private string _lineTheatersImage;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineTheatersImage
        {
            get
            {
                return _lineTheatersImage;
            }
            set
            {
                if (value != _lineTheatersImage)
                {
                    _lineTheatersImage = value;
                    NotifyPropertyChanged("LineTheatersImage");
                }
            }
        }

        private string _lineTheatersCast;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineTheatersCast
        {
            get
            {
                return _lineTheatersCast;
            }
            set
            {
                if (value != _lineTheatersCast)
                {
                    _lineTheatersCast = value;
                    NotifyPropertyChanged("LineTheatersCast");
                }
            }
        }

        private string _lineTheatersGenre;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineTheatersGenre
        {
            get
            {
                return _lineTheatersGenre;
            }
            set
            {
                if (value != _lineTheatersGenre)
                {
                    _lineTheatersGenre = value;
                    NotifyPropertyChanged("LineTheatersGenre");
                }
            }
        }

        private string _lineTheatersRating;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineTheatersRating
        {
            get
            {
                return _lineTheatersRating;
            }
            set
            {
                if (value != _lineTheatersRating)
                {
                    _lineTheatersRating = value;
                    NotifyPropertyChanged("LineTheatersRating");
                }
            }
        }



        private string _lineUpcomingTitle;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineUpcomingTitle
        {
            get
            {
                return _lineUpcomingTitle;
            }
            set
            {
                if (value != _lineUpcomingTitle)
                {
                    _lineUpcomingTitle = value;
                    NotifyPropertyChanged("LineUpcomingTitle");
                }
            }
        }

        private string _lineUpcomingImage;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineUpcomingImage
        {
            get
            {
                return _lineUpcomingImage;
            }
            set
            {
                if (value != _lineUpcomingImage)
                {
                    _lineUpcomingImage = value;
                    NotifyPropertyChanged("LineUpcomingImage");
                }
            }
        }

        private string _lineUpcomingCast;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineUpcomingCast
        {
            get
            {
                return _lineUpcomingCast;
            }
            set
            {
                if (value != _lineUpcomingCast)
                {
                    _lineUpcomingCast = value;
                    NotifyPropertyChanged("LineUpcomingCast");
                }
            }
        }

        private string _lineUpcomingGenre;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineUpcomingGenre
        {
            get
            {
                return _lineUpcomingGenre;
            }
            set
            {
                if (value != _lineUpcomingGenre)
                {
                    _lineUpcomingGenre = value;
                    NotifyPropertyChanged("LineUpcomingGenre");
                }
            }
        }

        private string _lineUpcomingRating;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineUpcomingRating
        {
            get
            {
                return _lineUpcomingRating;
            }
            set
            {
                if (value != _lineUpcomingRating)
                {
                    _lineUpcomingRating = value;
                    NotifyPropertyChanged("LineUpcomingRating");
                }
            }
        }


        // variables related to Watchlist
        private string _lineWatchlistTitle;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineWatchlistTitle
        {
            get
            {
                return _lineWatchlistTitle;
            }
            set
            {
                if (value != _lineWatchlistTitle)
                {
                    _lineWatchlistTitle = value;
                    NotifyPropertyChanged("LineWatchlistTitle");
                }
            }
        }

        private string _lineWatchlistImage;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineWatchlistImage
        {
            get
            {
                return _lineWatchlistImage;
            }
            set
            {
                if (value != _lineWatchlistImage)
                {
                    _lineWatchlistImage = value;
                    NotifyPropertyChanged("LineWatchlistImage");
                }
            }
        }

        private string _lineWatchlistCast;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineWatchlistCast
        {
            get
            {
                return _lineWatchlistCast;
            }
            set
            {
                if (value != _lineWatchlistCast)
                {
                    _lineWatchlistCast = value;
                    NotifyPropertyChanged("LineWatchlistCast");
                }
            }
        }

        private string _lineWatchlistGenre;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineWatchlistGenre
        {
            get
            {
                return _lineWatchlistGenre;
            }
            set
            {
                if (value != _lineWatchlistGenre)
                {
                    _lineWatchlistGenre = value;
                    NotifyPropertyChanged("LineWatchlistGenre");
                }
            }
        }

        private string _lineWatchlistRating;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineWatchlistRating
        {
            get
            {
                return _lineWatchlistRating;
            }
            set
            {
                if (value != _lineWatchlistRating)
                {
                    _lineWatchlistRating = value;
                    NotifyPropertyChanged("LineWatchlistRating");
                }
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public bool Equals(ItemViewModel m)
        {
            return m.LineId == this.LineId;
        }
    }
}